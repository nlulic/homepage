/** @type {import('next').NextConfig} */
const nextConfig = {
    reactStrictMode: true,
    env: {
        BUILD_YEAR: new Date().getFullYear().toString(),
    },
};

module.exports = nextConfig;
