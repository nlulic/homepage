import { Html, Head, Main, NextScript } from "next/document";

const Document = () => (
    <Html lang="en">
        <Head />
        <body className="bg-white text-gray-800 dark:bg-gray-800 dark:text-white">
            <script
                dangerouslySetInnerHTML={{ __html: presetThemeMinJs }}
            ></script>

            <Main />
            <NextScript />
        </body>
    </Html>
);

const presetThemeMinJs = `("dark"===localStorage.theme||window.matchMedia("(prefers-color-scheme: dark)").matches)&&document.documentElement.classList.add("dark");`;

export default Document;
