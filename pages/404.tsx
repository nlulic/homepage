import type { NextPage } from "next";

const NotFound: NextPage = () => (
    <section className="container py-32 flex justify-center items-center">
        <h1 className="text-2xl py-2 pr-6 border-r mr-4 font-semibold">404</h1>
        <h2 className="text-sm">This page could not be found</h2>
    </section>
);

export default NotFound;
