import type { NextPage } from "next";
import { Chip, Tag } from "components";

const Home: NextPage = () => (
    <>
        <article className="prose dark:prose-invert">
            <section>
                <header>
                    <h2>Me</h2>
                </header>
                <p className="prose-xl">
                    Hi 👋, I like to build software and help teams ship lovable
                    and user centered products.
                </p>

                <p>I speak German, English and Croatian.</p>
            </section>

            <section>
                <header>
                    <h2>Skills</h2>
                    <p>
                        A list of <b>languages</b>, <b>frameworks</b> and{" "}
                        <b>tools</b> I am using regularly and feel comfortable
                        in. However, I am not tied to specific technologies as
                        tools to build software evolve constantly.
                    </p>
                </header>

                <section>
                    <header>
                        <h3>Languages</h3>
                    </header>

                    <ul className="list-none p-0">
                        {["TypeScript", "JavaScript", "Go", "SQL"].map(
                            (txt, i) => (
                                <li
                                    key={i}
                                    className="inline-block mt-0 mb-2 mr-2 pl-0"
                                >
                                    <Chip>{txt}</Chip>
                                </li>
                            )
                        )}
                    </ul>
                </section>

                <section>
                    <header>
                        <h3>Frameworks</h3>
                    </header>

                    <ul className="list-none p-0">
                        {[
                            "React",
                            "Angular",
                            "Next.js",
                            "Express",
                            "Gin",
                            "Tailwind",
                            "Material UI",
                            "Bootstrap",
                        ].map((txt, i) => (
                            <li
                                key={i}
                                className="inline-block mt-0 mb-2 mr-2 pl-0"
                            >
                                <Chip>{txt}</Chip>
                            </li>
                        ))}
                    </ul>
                </section>

                <section>
                    <header>
                        <h3>Platforms / Tools</h3>
                    </header>

                    <ul className="list-none p-0">
                        {[
                            "Jest",
                            "Playwright",
                            "Jenkins",
                            "Docker",
                            "OpenAPI",
                            "Redis",
                            "Node.js",
                            "Git",
                            "NPM",
                            "Yarn",
                            "Webpack",
                        ].map((txt, i) => (
                            <li
                                key={i}
                                className="inline-block mt-0 mb-2 mr-2 pl-0"
                            >
                                <Chip>{txt}</Chip>
                            </li>
                        ))}
                    </ul>
                </section>
            </section>

            <section>
                <header>
                    <h2>Work</h2>
                    <p>
                        A summary of the professional experience I've gained so
                        far working as a software developer.
                    </p>
                </header>
                <ol className="relative list-none border-l border-gray-800 dark:border-white">
                    <li className="mb-10 before:w-3 before:h-3 before:block before:rounded-full before:absolute before:mt-2 before:-left-1.5 before:outline before:outline-8 before:outline-white before:bg-gray-800 dark:before:bg-white dark:before:outline-gray-800">
                        <time>2022 - now</time>
                        <section>
                            <header>
                                <h3 className="my-2">Software Engineer</h3>
                                <h4 className="font-normal">
                                    durchblicker.at - YOUSURE Tarifvergleich
                                    GmbH
                                </h4>
                            </header>
                            <p>
                                durchblicker.at is Austria's leading independent
                                price comparison portal in the areas of
                                insurance, telecommunications, energy and
                                finance.
                            </p>

                            <p>
                                Keeping up with the constantly changing business
                                areas requires flexible and easily extensible
                                software and infrastructure.
                            </p>

                            <p>
                                Among other things, I've been working on
                                improving our custom form generator library. It
                                is responsible for creating all pages where
                                users can submit their information on which all
                                further price calculations are based.{" "}
                            </p>
                        </section>
                        {[
                            "React",
                            "Next.js",
                            "Express",
                            "Jest",
                            "Redis",
                            "Headless WP",
                            "GCP",
                        ].map((value, i) => (
                            <Tag key={i} className="mr-2 mb-1">
                                {value}
                            </Tag>
                        ))}
                    </li>
                    <li className="mb-10 before:w-2 before:h-2 before:block before:rounded-full before:absolute before:mt-2 before:-left-1 before:outline before:outline-8 before:outline-white before:bg-gray-800 dark:before:bg-white dark:before:outline-gray-800">
                        <time>2020 - 2022</time>
                        <section>
                            <header>
                                <h3 className="my-2">Software Engineer</h3>
                                <h4 className="font-normal">
                                    adesso Austria GmbH
                                </h4>
                            </header>
                            <p>
                                I have spent most of my time at adesso working
                                on a customer portal for the austrian chambers
                                of agriculture in which most of the
                                communication between advisors and members can
                                take place. Among other things, members can use
                                the web application to book events, manage
                                appointments, obtain or renew qualifications,
                                receive news based on their interests and easily
                                update their user information.
                            </p>
                            <p>
                                In our small team I was mainly focused on the
                                implementation of technical concepts and
                                features in the projects frontend while also
                                maintaining our CI/CD pipeline.
                            </p>
                            <p>
                                Later in the project I've become responsible for
                                the entire frontend development.
                            </p>
                        </section>
                        {[
                            "Angular",
                            "Spring MVC",
                            "OpenAPI",
                            "Kafka",
                            "Material UI",
                            "Jenkins",
                        ].map((value, i) => (
                            <Tag key={i} className="mr-2 mb-1">
                                {value}
                            </Tag>
                        ))}
                    </li>
                    <li className="mb-10 before:w-2 before:h-2 before:block before:rounded-full before:absolute before:mt-2 before:-left-1 before:outline before:outline-8 before:outline-white before:bg-gray-800 dark:before:bg-white dark:before:outline-gray-800">
                        <time>2016 - 2020</time>
                        <section>
                            <section>
                                <header>
                                    <h3 className="my-2 mr-3 sm:inline-block">
                                        Software Developer
                                    </h3>
                                    <time className="text-sm">2019 - 2020</time>
                                    <h4 className="font-normal mt-0">
                                        iXTS GmbH
                                    </h4>
                                </header>
                                <p>
                                    Conceptual design, planning and development
                                    of a web application for evaluation and
                                    visualization of completed sprints in JIRA
                                    to improve the planning of future sprints
                                    and roadmaps
                                </p>
                                <p>
                                    Implementation of a CI/CD pipeline and
                                    multiple test environments for all existing
                                    software projects
                                </p>
                                <p>
                                    Partial migration of legacy modules written
                                    with Angular.js 1.X to React
                                </p>
                            </section>
                            <hr className="my-3" />
                            <section>
                                <header>
                                    <h3 className="my-2 mr-3 sm:inline-block">
                                        Junior Software Developer
                                    </h3>
                                    <time className="text-sm">2016 - 2019</time>
                                    <h4 className="font-normal mt-0">
                                        iXTS GmbH
                                    </h4>
                                </header>
                                <p>
                                    Design and development of a CLI for
                                    automated deployment monitoring, as well as
                                    integration into the existing CI/CD pipeline
                                </p>
                                <p>
                                    Development of a tariff calculator including
                                    its checkout process
                                </p>
                                <p>
                                    Improvement and development of features in
                                    existing software projects
                                </p>
                            </section>
                        </section>
                        {[
                            "React",
                            "Angular",
                            "AngularJS",
                            ".NET",
                            "Express",
                            "Bootstrap",
                            "Jest",
                            "Jenkins",
                        ].map((value, i) => (
                            <Tag key={i} className="mr-2 mb-1">
                                {value}
                            </Tag>
                        ))}
                    </li>
                </ol>
            </section>
        </article>
    </>
);

export default Home;
