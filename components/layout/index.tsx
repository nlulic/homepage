import { FC, ReactNode } from "react";
import Head from "next/head";
import Link from "next/link";

const Layout: FC<{ children: ReactNode }> = ({ children }) => {
    const toggleTheme = () => {
        const html = document.documentElement;
        html.classList.toggle("dark");

        html.classList.contains("dark")
            ? localStorage.setItem("theme", "dark")
            : localStorage.removeItem("theme");
    };

    return (
        <>
            <Head>
                <title>Nikola Lulić</title>
                <link rel="icon" href="/favicon.ico" />
                <meta
                    name="description"
                    content="Hi, I'm a software engineer. Welcome to my home on the internet."
                />
            </Head>

            <header className="container mb-16 py-6 flex justify-between items-center">
                <Link href="/">
                    <a className="uppercase text-sm font-bold tracking-widest ">
                        Nikola Lulić
                    </a>
                </Link>

                <div className="flex items-center">
                    <button
                        title="Toggle theme"
                        onClick={toggleTheme}
                        className="transition border-4 h-4 w-8 rounded-full bg-gray-800 border-gray-800 dark:bg-white dark:border-white"
                    >
                        <span className="transition rounded-full block w-2 h-2 bg-white border-white dark:bg-gray-800 dark:border-gray-800 dark:translate-x-4"></span>
                    </button>
                </div>
            </header>

            <main className="container">{children}</main>

            <footer className="container font-display mx-auto px-4 text-sm my-12 text-center md:text-xs">
                <nav className="w-full mb-4">
                    <ul className="flex justify-center tracking-wide space-x-4">
                        <li>
                            <a
                                href="https://linkedin.com/in/nikola-lulic-8a22a9192"
                                rel="noopener noreferrer"
                                target="_blank"
                            >
                                LinkedIn
                            </a>
                        </li>
                        <li>
                            <a
                                href="https://gitlab.com/nlulic"
                                rel="noopener noreferrer"
                                target="_blank"
                            >
                                GitLab
                            </a>
                        </li>
                    </ul>
                </nav>

                <p className="tracking-wide">
                    ©{process.env.BUILD_YEAR} Nikola Lulić
                </p>
            </footer>
        </>
    );
};

export default Layout;
