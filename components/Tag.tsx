import { FC, PropsWithChildren } from "react";

const Tag: FC<PropsWithChildren<{ className?: string }>> = ({
    children,
    className = "",
}) => (
    <aside
        className={`${className} text-white bg-gray-800 text-xs px-2 py-1 font-semibold inline-block rounded-2xl dark:text-gray-800 dark:bg-white`}
    >
        {children}
    </aside>
);

export default Tag;
