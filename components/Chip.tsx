import { FC, ReactNode } from "react";

const Chip: FC<{ children: ReactNode }> = ({ children }) => (
    <span className="whitespace-nowrap px-4 py-2 rounded inline-block font-semibold text-sm text-white bg-gray-800 dark:text-gray-800 dark:bg-white">
        {children}
    </span>
);

export default Chip;
