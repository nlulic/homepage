module.exports = {
    content: ["./pages/**/*.{ts,tsx}", "./components/**/*.{ts,tsx}"],
    darkMode: "class",
    theme: {
        container: {
            center: true,
            padding: {
                DEFAULT: "2rem",
                sm: "3rem",
                lg: "4rem",
                xl: "6rem",
                "2xl": "8rem",
            },
        },
        extend: {},
    },
    plugins: [
        require("@tailwindcss/typography"),
        require("@tailwindcss/line-clamp"),
    ],
};
